#!/usr/bin/env python3
from flask import Flask, render_template, request, redirect, url_for
import os
import threading
import time
from picamera2 import Picamera2

app = Flask(__name__, static_folder='/home/flora/floraguard/static')
camera = Picamera2()
camera.start()

# Shared resource lock
lock = threading.Lock()
save_path = "/home/flora/floraguard/static/images"  # Default path within static directory
capture_frequency = 60  # Default frequency in seconds

def capture_images():
    while True:
        lock.acquire()
        path = save_path
        frequency = capture_frequency
        lock.release()
        timestamp = time.strftime("%Y%m%d-%H%M%S")
        filename = f"{timestamp}.jpg"
        file_path = os.path.join(path, filename)
        camera.capture_file(file_path)
        time.sleep(frequency)

# Start the camera in a separate thread
t = threading.Thread(target=capture_images)
t.daemon = True
t.start()

def find_latest_image(path):
    try:
        files = [os.path.join(path, f) for f in os.listdir(path) if f.endswith('.jpg')]
        if files:
            latest_file = max(files, key=os.path.getctime)
            return os.path.basename(latest_file)
    except Exception as e:
        print(f"Error finding latest image: {e}")
    return None

@app.route('/', methods=['GET', 'POST'])
def index():
    global save_path, capture_frequency
    if request.method == 'POST':
        lock.acquire()
        save_path = request.form['path']
        capture_frequency = int(request.form['frequency'])
        lock.release()
    latest_image = find_latest_image(save_path)
    return render_template('index.html', path=save_path, frequency=capture_frequency, image_file=latest_image)

@app.route('/start-capture')
def start_capture():
    # Redirect back to home page as the capture is already continuously running
    return redirect(url_for('index'))

@app.route('/latest-image')
def latest_image():
    latest_image = find_latest_image(save_path)
    if latest_image:
        return url_for('static', filename='images/' + latest_image)
    return url_for('static', filename='images/no-image.png')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)
