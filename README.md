# PiVision_tools
Projects/tools for computer vision using Raspberry Pi SBCs

## FloraGuard

![image info](Doc/FloraGuard.PNG)

FloraGuard leverages Raspberry Pi and YOLO object detection models to monitor pollinizers like bees and hummingbirds visiting flowers. This initiative aims to enhance environmental research by automating data collection and analysis of pollinator activities, which are crucial for ecological assessments and conservation efforts. The project utilizes video data provided by Dr. Rossana Maguiña, processed through advanced machine learning techniques to accurately identify and track pollinator interactions in natural habitats.

## Usage

### WiFi Router Setup Script for Raspberry Pi Zero 2 W

#### Description
The script sets up the Raspberry Pi to function as a WiFi access point using `hostapd` and `dnsmasq` for managing connections and DHCP respectively. It configures `iptables` to handle network routing and masquerading, ensuring devices connected to the Pi can access the internet through its network interface.

#### Requirements
- Raspberry Pi Zero 2 W
- Raspberry Pi OS Lite 64-bit (2024-07-04 release)
- Internet connection to download necessary packages
- SSH or direct access to the terminal

#### Usage
1. **Make the script executable**:
 ```bash
chmod +x setup_wifi_router.sh
```
2. **Run the script with sudo** and provide your desired SSID and password:
 ```bash
sudo ./setup_wifi_router.sh "YourSSID" "YourStrongPassword"
```
3. **Reboot**

### Flask Camera App Service

#### Description
This Flask application provides a web interface for controlling a camera connected to a Raspberry Pi. It allows users to capture and view images directly through the web interface. The application is configured to start automatically at boot using a systemd service.

#### Flask Service Setup Script

##### Description
The provided script automates the setup of the Raspberry Pi to run the Flask application as a systemd service. This setup ensures that the Flask application starts on boot and remains running.

##### Requirements
- Raspberry Pi with Raspberry Pi OS installed.
- Flask application must be installed and configured correctly in the specified directory.
- User must have `sudo` privileges to configure systemd services.

##### Usage
1. **Make the script executable**:
```bash
   chmod +x setup_flask_service.sh
```
2. **Run the script with sudo** and provide your desired service name and the path to your Flask application directory:
```bash
sudo ./setup_flask_service.sh "floraguard" "/home/flora/pivision_tools/Code"
```
3. **Reboot**